#!/usr/bin/env python


from libs.MainApplication import MainApplication


def main():
    MainApplication()


if __name__ == '__main__':
    main()
