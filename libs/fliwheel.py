#! /usr/bin/env python

import time
import tkinter as Tk
from threading import Thread
from .gui import GUIElement
from .fli_filter_wheel import USBFilterWheel


class FLIWheel(GUIElement):
    def __init__(self, window):
        super().__init__(window, "FLI")

        # Pack buttons for filter selection
        self.filters = {"X": 5, "Y": 6, "Empty": 7}
        self.num_of_filters = len(self.filters)
        self.filter_buttons = {}
        for idx, filter_name in enumerate(self.filters.keys()):
            button = Tk.Button(self.panel, text=filter_name, width=8, font=("Arial", 10, "bold"),
                               command=lambda x=filter_name: self.callback(x))
            self.filter_buttons[filter_name] = button
            self.filter_buttons[filter_name].grid(column=idx, row=1, padx=3)

        # Status message label
        self.statusMessage = Tk.StringVar()
        Tk.Label(self.panel, textvariable=self.statusMessage, width=6).grid(column=self.num_of_filters + 1,
                                                                            row=1, padx=5)

        # Conntect to FLI USB filter wheel
        self.device = USBFilterWheel.find_devices()[0]
        self.is_alive = True
        self.moving = False
        self.roll_filters()  # Select first and then empty empty slot

    def roll_filters(self):
        def f():
            time.sleep(1)
            self.set_filter(0)
            self.set_filter("Empty")
        Thread(target=f).start()

    def shutdown(self):
        def f():
            self.device.set_filter_pos(0)
            self.is_alive = False
        Thread(target=f).start()

    def callback(self, filter_name):
        if self.moving is False:
            thread = Thread(target=self.set_filter, args=(filter_name, ))
            thread.start()

    def set_filter(self, filter_name):
        if str(filter_name).isnumeric():
            filter_index = filter_name
        else:
            filter_index = self.filters[filter_name]
        current_filter_index = self.device.get_filter_pos()
        if filter_index == current_filter_index:
            # The chosen filter is already selected
            return
        print("setting %s" % filter_name)
        self.moving = True
        # Shade button labels while we are moveing filters
        for button in self.filter_buttons:
            self.filter_buttons[button].config(fg="gray")
        self.statusMessage.set("Moving")

        # Set filter position
        self.device.set_filter_pos(filter_index)
        # Get filter position to ensure success
        new_filter_index = self.device.get_filter_pos()
        print(filter_name, filter_index, new_filter_index)
        # Enable buttons back
        for button in self.filter_buttons:
            self.filter_buttons[button].config(fg='black')
        if filter_name in self.filter_buttons:
            self.filter_buttons[filter_name].config(fg="red")
        self.moving = False
        self.statusMessage.set("OK")
