#! /usr/bin/env python

import tkinter as Tk


class GUIElement(Tk.Frame):
    """
    Base class for Telescope device management GUI
    """

    def __init__(self, window, name):
        self.window = window
        self.panel = Tk.Frame(self.window.root)
        self.panel.grid(sticky=[Tk.W, Tk.E], pady=3)
        self.device_label = Tk.Label(self.panel, text="  %s" % name, font=("Times", 14, "bold"))
        self.device_label.grid(column=0, columnspan=4, row=0, sticky=Tk.W)
