#! /usr/bin/env python

import serial
import time


class SerialDevice(object):
    def __init__(self, address, baudrate):
        self.device = serial.Serial(address, baudrate=baudrate, timeout=0.2)

    def send_command(self, command):
        self.device.flushOutput()
        self.device.write(str.encode(command))

    def get_response(self, lines_desired):
        """Read output until end_str is found"""
        response = []
        while len(response) < lines_desired:
            new_line = str(self.device.readline())
            response.append(new_line)
            time.sleep(0.05)
        return response

    def readline(self):
        byte_line = self.device.readline()
        return byte_line.decode("utf-8").strip()

    def close(self):
        self.device.close()
