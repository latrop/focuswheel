#! /usr/bin/env python

import tkinter as Tk
from threading import Thread
from .SerialDevice import SerialDevice
from .gui import GUIElement


class Focuser(GUIElement):
    def __init__(self, window):
        super().__init__(window, "Focuser")

        # Pack buttons, labels and entry
        # Position label
        self.focus_position = 0
        self.moving = False
        self.focus_position_TkValue = Tk.StringVar()
        self.focus_position_TkValue.set(self.focus_position)
        Tk.Label(self.panel, text="Current location:", font=("Times", 12)).grid(column=0, row=1, padx=10)
        Tk.Label(self.panel, textvariable=self.focus_position_TkValue, font=("Times", 12)).grid(column=1, row=1)

        # Status message
        self.status_message = Tk.StringVar()
        Tk.Label(self.panel, textvariable=self.status_message).grid(column=2, row=1)

        # Move left button
        self.move_left_button = Tk.Button(self.panel, text=u"\u2212", font=("Arial", 16, "bold"),
                                          command=lambda: self.callback("left"))
        self.move_left_button.grid(column=0, row=2, sticky=Tk.E, padx=15)

        # Move step entry
        self.step_variable = Tk.StringVar()
        self.step_variable.set(500)
        self.move_step_entry = Tk.Entry(self.panel, textvariable=self.step_variable, width=6, justify=Tk.RIGHT)
        self.move_step_entry.grid(column=1, row=2)

        # Move right button
        self.move_right_button = Tk.Button(self.panel, text="+", font=("Arial", 16, "bold"),
                                           command=lambda: self.callback("right"))
        self.move_right_button.grid(column=2, row=2, sticky=Tk.W, padx=15)

        # Initialise wheel
        self.device = SerialDevice("COM4", baudrate=9600)
        self.move(0, "right")
        self.is_alive = True

    def shutdown(self):
        self.device.close()
        self.is_alive = False

    def callback(self, direction):
        if self.moving is False:
            step = int(self.step_variable.get())
            thread = Thread(target=self.move, args=(step, direction))
            thread.start()

    def move(self, step, direction):
        if step > 1000:
            self.status_message.set("Too large step")
            return
        self.moving = True
        self.status_message.set("Moving")
        # Disable buttons
        self.move_left_button.config(state='disabled')
        self.move_right_button.config(state='disabled')
        if direction == "left":
            step = -1 * step
        command = "%i\n" % step
        self.device.send_command(command)
        # Read response untill current relative position is returned
        while 1:
            line = self.device.readline()
            if "position" in line:
                break
        self.moving = False
        # Find current position from the response
        self.focus_position = int(line.split()[-1])
        print(self.focus_position)
        self.focus_position_TkValue.set(self.focus_position)
        # Enable buttons back when movement is finished
        self.move_left_button.config(state='normal')
        self.move_right_button.config(state='normal')
        self.status_message.set("Status: OK")
