#! /usr/bin/env python

import tkinter as Tk
import time
from threading import Thread
from .SerialDevice import SerialDevice
from .gui import GUIElement


class CFW8(GUIElement):
    def __init__(self, window):
        self.device_name = "CFW8"
        super().__init__(window, self.device_name)

        # Pack buttons for filter selection
        self.filter_list = ["U", "B", "V", "R", "I"]
        self.num_of_filters = len(self.filter_list)
        self.current_filter = "U"
        self.moving = False
        self.filter_buttons = {}
        for idx, filter_name in enumerate(self.filter_list):
            button = Tk.Button(self.panel, text=filter_name, width=3, font=("Times", 12, "bold"),
                               command=lambda x=filter_name: self.callback(x))
            self.filter_buttons[filter_name] = button
            self.filter_buttons[filter_name].grid(column=idx, row=1, padx=5)
        # Highlight filter U button as it is selected
        self.filter_buttons["U"].config(fg="red")

        # Status message label
        self.statusMessage = Tk.StringVar()
        Tk.Label(self.panel, textvariable=self.statusMessage, width=6).grid(column=self.num_of_filters + 1,
                                                                            row=1, padx=5)

        # Initialise wheel
        self.device = SerialDevice("COM6", baudrate=115200)
        self.roll_filters()
        self.is_alive = True

    def roll_filters(self):
        def f():
            time.sleep(1.0)
            for filter_name in self.filter_list[1:]:
                self.set_filter(filter_name)
            self.set_filter(self.filter_list[0])
        Thread(target=f).start()

    def shutdown(self):
        def f():
            self.device_label.config(text="%s: shutting down" % self.device_name)
            self.set_filter("U")
            self.device.close()
            self.is_alive = False
        Thread(target=f).start()

    def callback(self, filter_name):
        if self.moving is False:
            thread = Thread(target=self.set_filter, args=(filter_name))
            thread.start()

    def set_filter(self, filter_name):
        """
        Set a particular filter to a filter wheel
        """
        print("setting %s" % filter_name)
        self.moving = True
        # Shade button labels while we are moveing filters
        for button in self.filter_buttons:
            if button != self.current_filter:
                self.filter_buttons[button].config(fg="gray")
        while self.current_filter != filter_name:
            current_filter_idx = self.filter_list.index(self.current_filter)
            next_filter = self.filter_list[(current_filter_idx + 1) % self.num_of_filters]
            # Set "moving" status
            self.statusMessage.set("Moving")
            self.device.send_command(next_filter)
            while 1:
                line = self.device.readline()
                if "done" in line:
                    self.statusMessage.set("OK")
                    break
                elif "Error" in line:
                    self.statusMessage.set("Error")
                    self.moving = False
                    break
            # Unset red color from the previous filter button
            self.filter_buttons[self.current_filter].config(fg="gray")
            self.current_filter = next_filter
            # Set red color to the current filter button
            self.filter_buttons[self.current_filter].config(fg="red")
        print("Done", self.current_filter)

        # Enable buttons back
        for button in self.filter_buttons:
            if button != self.current_filter:
                self.filter_buttons[button].config(fg='black')
        self.moving = False
