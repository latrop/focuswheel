#! /usr/bin/env python

import tkinter as Tk
from tkinter import messagebox
from .cfw8 import CFW8
from .fliwheel import FLIWheel
from .focuser import Focuser


class MainApplication(Tk.Frame):
    def __init__(self, *args, **kwargs):
        self.root = Tk.Tk()
        self.root.title("Wheels and focus control")
        self.root.protocol('WM_DELETE_WINDOW', self.shutdown)
        self.cfw8 = CFW8(self)
        self.fliWheel = FLIWheel(self)
        self.focuser = Focuser(self)
        self.root.mainloop()

    def shutdown(self):
        print("Shutting_down")
        answer = messagebox.askokcancel("Close", "Really close?")
        if answer is True:
            self.root.after(0, self.cfw8.shutdown)
            self.root.after(0, self.fliWheel.shutdown)
            self.root.after(0, self.focuser.shutdown)
            self.root.after(0, self.close)

    def close(self):
        # Set U-filter to CFW-8 wheel
        if (self.cfw8.is_alive is False) and (self.fliWheel.is_alive is False) and (self.focuser.is_alive is False):
            self.root.destroy()
        else:
            self.root.after(100, self.close)
